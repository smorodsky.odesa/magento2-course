<?php

namespace Training\TestOM\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;

class Index extends Action
{
    private $test;
    private $testCustom;

    public function __construct(
        Context $context,
        \Training\TestOM\Model\Test $test,
        \Training\TestOM\MOdel\CustomTest $testCustom
    )
    {
        $this->test = $test;
        $this->testCustom = $testCustom;
        parent::__construct($context);
    }

    public function execute()
    {
        $this->test->log();
        $this->testCustom->run();
        exit();
    }
}
