<?php


namespace Training\TestOM\Model;


class CustomTest
{
    private $test;
    private $testFactory;
    private $manager;

    public function __construct(
        \Training\TestOM\Model\Test $test,
        \Training\TestOM\Model\TestFactory $testFactory,
        \Training\TestOM\Model\ManagerCustomImplementation $manager
    )
    {
        $this->test = $test;
        $this->testFactory = $testFactory;
        $this->manager = $manager;
    }

    public function run()
    {
        $this->test->log();

        $customArray = ['item1' => 'testFactory1', 'item2' => 'testFactory2'];
        $newTestObject = $this->testFactory->create([
            'arrayList' => $customArray,
            'manager' => $this->manager
        ]);

        $newTestObject->log();
    }

}
