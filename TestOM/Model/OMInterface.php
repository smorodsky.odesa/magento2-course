<?php

namespace Training\TestOM\Model;

interface OMInterface
{
    public function create();
    public function get();
}
