<?php


namespace Training\TestOM\Model;


class Test
{
    private $manager;
    private $name;
    private $arrayList;
    private $number;
    private $managerFactory;

    public function __construct(
        \Training\TestOM\Model\OMInterface $manager,
        $name,
        int $number,
        array $arrayList,
        \Training\TestOM\Model\OMInterfaceFactory $managerFactory

    ){
        $this->arrayList = $arrayList;
        $this->manager = $manager;
        $this->name = $name;
        $this->number = $number;
        $this->managerFactory = $managerFactory;
    }

    public function log() {
        print_r(get_class($this->manager));
        echo "<br>";
        print_r($this->number);
        echo "<br>";
        print_r($this->name);
        echo "<br>";
        print_r($this->arrayList);
        echo "<br>";
        $newManager = $this->managerFactory->create();
        print_r($newManager);
    }
}
