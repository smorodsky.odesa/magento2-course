<?php

namespace Training\TestControllers\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class LogPage implements ObserverInterface {

    /**
     * @var \Psr\Log\LoggerInterface
    */
    private $logger;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Psr\Log\LoggerInterface $logger
    ){
        $this->logger = $logger;
    }

    public function execute(Observer $observer)
    {
        // TODO: Implement execute() method.
        $response = $observer->getEvent()->getData('request');
        $this->logger->debug($response->getBody());
    }
}
