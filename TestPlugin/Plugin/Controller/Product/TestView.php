<?php

namespace Training\TestPlugin\Plugin\Controller\Product;

class TestView {
    protected $customerSession;
    protected $redirectFactory;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectFactory
    ){
        $this->redirectFactory = $redirectFactory;
        $this->customerSession = $customerSession;
    }

    public function aroundExecute(
        \Magento\Catalog\Controller\Product\View $view,
        \Closure $proceed
    ){
        if (!$this->customerSession->isLoggedIn()) {
            return $this->redirectFactory->create()->setPath('customer/account/login');
        }

        return $proceed;
    }
}
