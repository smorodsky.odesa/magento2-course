<?php

namespace Training\TestPlugin\Plugin\Model;

class Url
{
    public function beforeGetUrl(
        \Magento\Framework\UrlInterface $url,
        $routePath = null,
        $routeParams = null
    ){
        if ($routePath == 'customer/account/create') {
            return ['customer/account/login', null ];
        }
    }
}
