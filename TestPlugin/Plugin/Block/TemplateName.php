<?php

namespace Training\TestPlugin\Plugin\Block;

class TemplateName {
    public function afterToHtml(
        \Magento\Framework\View\Element\Template $template,
        $result
    ){
        if ($template->getNameInLayout() == 'top.search') {
            $result = '<div>' .
                '<p>' . $template->getTemplate() . '</p>' .
                '<p>' . get_class($template) . '</p>' . '</div>';
        }

        return $result;
    }
}
