<?php

namespace Training\TestRender\Block;

class TestBlock extends \Magento\Framework\View\Element\AbstractBlock {
    public function _toHtml()
    {
        return "<b>TestBlock</b>";
    }
}
