<?php

namespace Training\TestRender\Controller\Block;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Training\TestRender\Block\TestBlock;

class Index implements HttpGetActionInterface
{
    private $layoutFactory;
    private $resultRawFactory;

    public function __construct(
        \Magento\Framework\View\LayoutFactory $layoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory

    )
    {
        $this->layoutFactory = $layoutFactory;
        $this->resultRawFactory = $resultRawFactory;
    }

    public function execute()
    {
        // TODO: Implement execute() method.
        $layout = $this->layoutFactory->create();
        $block = $layout->createBlock(TestBlock::class);

        $resultRaw = $this->resultRawFactory->create();
        $resultRaw->setHeader('Content-Type', 'text', true);
        $resultRaw->setContents($block->toHtml());
        //$this->getResponse()->appendBody($block->toHtml());

        return $resultRaw;
    }
}
